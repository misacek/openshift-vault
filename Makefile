
LABEL_BUILD_DATE := $(shell date --rfc-3339=seconds)

SERVICE_NAME ?= vault

HAVE_GIT := $(shell git --version 2>/dev/null)
ifdef HAVE_GIT
LAST_COMMIT := $(shell git rev-parse --short HEAD)
LAST_COMMIT_UNIX_TIMESTAMP := $(shell git show -s --format=%ct $(LAST_COMMIT))
LAST_COMMIT_DATE := $(shell date --date=@$(LAST_COMMIT_UNIX_TIMESTAMP) +%Y%m%d%H%M%S)
else
LAST_COMMIT := unknown-commit
endif

LABEL_BUILD_DATE := $(shell date --rfc-3339=seconds)
LABEL_VERSION := $(LAST_COMMIT_DATE)-$(LAST_COMMIT)

BUILD_ENV :=
BUILD_ENV += --build-arg LABEL_BUILD_DATE="$(LABEL_BUILD_DATE)"
BUILD_ENV += --build-arg LABEL_VCS_REF="$(LAST_COMMIT)"

build:
	docker-compose build $(BUILD_ENV) vault

start: run
run:
	docker-compose up -d $(SERVICE_NAME)
	docker-compose logs $(SERVICE_NAME)
	@echo "You should be able to access vault on http://127.0.0.1:8200"

stop:
	docker-compose stop

push:
	docker-compose push $(SERVICE_NAME)

.PHONY: test
