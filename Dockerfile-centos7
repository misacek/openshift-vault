FROM openshift/base-centos7

ARG LABEL_BUILD_DATE
ARG LABEL_URL=https://vault-test.cloud.upshift.engineering.redhat.com
ARG LABEL_VCS_REF
ARG LABEL_VCS_TYPE=git
ARG LABEL_VCS_URL=https://gitlab.cee.redhat.com/openshift/vault
ARG UID=5001

LABEL \
    io.openshift.expose-services="8200:8200" \
    io.openshift.tags="HashiCorp vault" \
    com.redhat.cluster-qa.vault.build_date="$LABEL_BUILD_DATE" \
    com.redhat.cluster-qa.vault.name="HashiCorp Vault" \
    com.redhat.cluster-qa.vault.summary="This is image for running \
production version of HashiCorp Vault in Openshift." \
    com.redhat.cluster-qa.vault.url="$LABEL_URL" \
    com.redhat.cluster-qa.vault.vcs-type="$LABEL_VCS_TYPE" \
    com.redhat.cluster-qa.vault.vcs-url="$LABEL_VCS_URL" \
    com.redhat.cluster-qa.vault.vcs-ref="$LABEL_VCS_REF" \
    maintainer="Michal Nováček <mnovacek@redhat.com>"

# This is the release of Vault to pull in.
ENV VAULT_VERSION=0.10.1

# This is the release of https://github.com/hashicorp/docker-base to pull in order
# to provide HashiCorp-built versions of basic utilities like dumb-init and gosu.
ENV DOCKER_BASE_VERSION=0.0.4

# Build directory in the container. Removed in the end.
ENV BUILD_DIR=/tmp/build

# Create a vault user and group first so the IDs get set the same way,
# even as the rest of this may change over time.
# modified from adduser and addgroup to centos commands. -r creates a system user, -g designates group
RUN groupadd vault && \
    useradd -r -g vault -g root -u $UID vault

# Set up certificates, our base tools, and Vault.
# Largely skipping certs for now. apt package manager commands replaced with yum
RUN \
    yum -y install ca-certificates gnupg openssl libcap jq && \
    curl https://keybase.io/hashicorp/pgp_keys.asc | gpg --import && \
    mkdir -p $BUILD_DIR

RUN \
    wget -P $BUILD_DIR \
        https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_linux_amd64.zip \
        https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS \
        https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS.sig && \
    gpg --batch --verify \
        $BUILD_DIR/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS.sig \
        $BUILD_DIR/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS
RUN \
    (cd $BUILD_DIR; grep ${DOCKER_BASE_VERSION}_linux_amd64.zip $BUILD_DIR/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS | sha256sum -c) && \
    unzip -d $BUILD_DIR $BUILD_DIR/docker-base_${DOCKER_BASE_VERSION}_linux_amd64.zip && \
    cp $BUILD_DIR/bin/gosu $BUILD_DIR/bin/dumb-init /bin


RUN \
    wget -P $BUILD_DIR \
        https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip \
        https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_SHA256SUMS \
        https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_SHA256SUMS.sig && \
    gpg --batch --verify \
        $BUILD_DIR/vault_${VAULT_VERSION}_SHA256SUMS.sig $BUILD_DIR/vault_${VAULT_VERSION}_SHA256SUMS

RUN \
    (cd $BUILD_DIR; grep vault_${VAULT_VERSION}_linux_amd64.zip $BUILD_DIR/vault_${VAULT_VERSION}_SHA256SUMS | sha256sum -c ) && \
    unzip -d /bin $BUILD_DIR/vault_${VAULT_VERSION}_linux_amd64.zip


RUN \
    rm -rf $BUILD_DIR /root/.gnupg &&\
    yum clean all && \
    mkdir -p /vault/logs /vault/storage /vault/config && \
    chmod 775 /vault/config /vault/storage /vault/logs && \
    chown -R vault:root /vault

VOLUME /vault/logs
VOLUME /vault/storage

EXPOSE 8200

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY serverconfig.hcl /vault/config/
RUN \
    chmod +x /usr/local/bin/docker-entrypoint.sh && \
    chown -R vault:root /vault/config/serverconfig.hcl
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["server", "-dev"]

USER $UID
WORKDIR /vault
